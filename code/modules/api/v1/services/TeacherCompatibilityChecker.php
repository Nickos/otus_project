<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use yii\helpers\ArrayHelper;

class TeacherCompatibilityChecker
{
    public function execute(Group $group, User $teacher): bool
    {
        $userSkills = ArrayHelper::map($teacher->skills, 'skill_id', 'skill_id');
        $groupSkills = ArrayHelper::map($group->skills, 'skill_id', 'skill_id');
        $groupSkillsCount = count($groupSkills);
        foreach ($groupSkills as $key => $groupSkill) {
            if (in_array($groupSkill, $userSkills)) {
                unset($groupSkills[$key]);
            }
        }
        $newGroupSKillCount = count($groupSkills);
        $percent = 100 * $newGroupSKillCount / $groupSkillsCount;
        $percent = 100 - round(floor($percent / 10) * 10);
        return $percent >= User::TEACHER_COMPATIBILITY_PERCENT;
    }
}