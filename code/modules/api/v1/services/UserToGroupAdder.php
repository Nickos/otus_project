<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\jobs\SelectTeacherJob;
use app\modules\api\v1\models\Group;
use app\modules\api\v1\repositories\GroupApiRepository;
use app\modules\api\v1\repositories\UserApiRepository;
use app\modules\api\v1\repositories\UserGroupApiRepository;
use app\modules\api\v1\services\notifier\Notifier;
use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

class UserToGroupAdder
{
    /**
     * @var \app\modules\api\v1\repositories\GroupApiRepository
     */
    private GroupApiRepository $groups;
    /**
     * @var \app\modules\api\v1\repositories\UserGroupApiRepository
     */
    private UserGroupApiRepository $userGroups;
    /**
     * @var \app\modules\api\v1\repositories\UserApiRepository
     */
    private UserApiRepository $users;
    /**
     * @var \app\modules\api\v1\services\notifier\Notifier
     */
    private Notifier $notifier;

    public function __construct(
        GroupApiRepository $groups,
        UserGroupApiRepository $userGroups,
        UserApiRepository $users,
        Notifier $notifier
    )
    {
        $this->groups = $groups;
        $this->userGroups = $userGroups;
        $this->users = $users;
        $this->notifier = $notifier;
    }

    public function execute(int $userId, int $groupId): bool
    {
        try {
            $this->checkIfGroupInBlock($userId, $groupId);
            GroupBlocker::blockGroup($groupId);
            $group = $this->groups->get($groupId);
            $user = $this->users->get($userId);
            $countUsersInGroup = $group->user_count;
            if ($countUsersInGroup >= Group::MAX_USER_COUNT) {
                GroupBlocker::unblockGroup($groupId);
                throw new Exception('The group is full.');
            }
            $isCreated = $this->userGroups->create($userId, $groupId);
            if ($isCreated) {
                $this->groups->updateCount($groupId);
                $this->notifier->notifyUserAddedToGroup($user, $group);
            }
            if (++$countUsersInGroup == Group::MAX_USER_COUNT) {
                Yii::$app->selectTeacherQueue->push(new SelectTeacherJob([
                    'group' => $group
                ]));
            }
            GroupBlocker::unblockGroup($groupId);
            return true;
        } catch (NotFoundHttpException $notFound) {
            Yii::warning($notFound->getMessage());
        } catch (Exception $exception) {
            Yii::warning($exception->getMessage());
        }
        return true;
    }

    private function checkIfGroupInBlock(int $userId, int $groupId)
    {
        if (GroupBlocker::isGroupInBlock($groupId)) {
            sleep(5);
            GroupBlocker::unblockGroup($groupId);
            self::execute($userId, $groupId);
        }
    }
}