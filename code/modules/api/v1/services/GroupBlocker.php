<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\helpers\cache\CacheHelperInterface;
use Yii;

class GroupBlocker
{
    public static function isGroupInBlock(int $groupId): bool
    {
        $cache = Yii::$container->get(CacheHelperInterface::class);
        return $cache->exist('group_in_block_' . $groupId);
    }

    public static function blockGroup(int $groupId): bool
    {
        $cache = Yii::$container->get(CacheHelperInterface::class);
        return $cache->add('group_in_block_' . $groupId, true, Yii::$app->params['group_block_time']);
    }

    public static function unblockGroup(int $groupId): bool
    {
        $cache = Yii::$container->get(CacheHelperInterface::class);
        return $cache->delete('group_in_block_' . $groupId);
    }
}