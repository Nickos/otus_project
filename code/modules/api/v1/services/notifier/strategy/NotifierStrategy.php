<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\strategy;

use app\modules\api\v1\dto\NotifyDto;
use app\modules\api\v1\models\User;
use app\modules\api\v1\services\notifier\factory\AbstractNotifierFactory;
use app\modules\api\v1\services\notifier\factory\EmailNotifierFactory;

class NotifierStrategy implements NotifierStrategyInterface
{
    public function selectNotifier(NotifyDto $dto): AbstractNotifierFactory
    {
        if ($dto->getUser()->notify_type == User::EMAIL_NOTIFICATION) {
            return new EmailNotifierFactory();
        }
    }
}