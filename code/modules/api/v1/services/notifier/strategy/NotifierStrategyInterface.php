<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\strategy;

use app\modules\api\v1\dto\NotifyDto;
use app\modules\api\v1\services\notifier\factory\AbstractNotifierFactory;

interface NotifierStrategyInterface
{
    public function selectNotifier(NotifyDto $dto): AbstractNotifierFactory;
}