<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier;

use app\modules\api\v1\dto\GroupSelectedFailDto;
use app\modules\api\v1\dto\GroupSelectedSuccessDto;
use app\modules\api\v1\dto\NotifyDto;
use app\modules\api\v1\dto\TeacherFoundDto;
use app\modules\api\v1\dto\UserAddedToGroupDto;
use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use app\modules\api\v1\services\notifier\strategy\NotifierStrategy;

class Notifier
{
    public function notifyGroupSelectedSuccess(User $user, Group $group): bool
    {
        $dto = new GroupSelectedSuccessDto($user, $group);
        return $this->buildNotifier($dto);
    }

    public function notifyGroupSelectedFail(User $user): bool
    {
        $dto = new GroupSelectedFailDto($user);
        return $this->buildNotifier($dto);
    }

    public function notifyUserAddedToGroup(User $user, Group $group)
    {
        $dto = new UserAddedToGroupDto($user, $group);
        return $this->buildNotifier($dto);
    }

    public function notifyTeacherFound(array $users): bool
    {
        foreach ($users as $user) {
            $dto = new TeacherFoundDto($user);
            $this->buildNotifier($dto);
        }
        return true;
    }

    private function buildNotifier(NotifyDto $dto)
    {
        $strategy = new NotifierStrategy();
        $notifierFactory = $strategy->selectNotifier($dto);
        $notifier = $notifierFactory->createNotifier($dto);
        return $notifier->send();
    }
}