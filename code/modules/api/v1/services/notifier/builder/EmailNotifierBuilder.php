<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\builder;

use app\modules\api\v1\services\notifier\factory\EmailNotifier;

class EmailNotifierBuilder
{
    private string $subject;

    private string $from;

    private array $to;

    private string $text;

    public function build(): EmailNotifier
    {
        return new EmailNotifier(
            $this->subject,
            $this->from,
            $this->to,
            $this->text
        );
    }

    public function setFrom(string $from): EmailNotifierBuilder
    {
        $this->from = $from;
        return $this;
    }

    public function setTo(array $to): EmailNotifierBuilder
    {
        $this->to = $to;
        return $this;
    }

    public function setSubject(string $subject): EmailNotifierBuilder
    {
        $this->subject = $subject;
        return $this;
    }

    public function setText(string $text): EmailNotifierBuilder
    {
        $this->text = $text;
        return $this;
    }
}