<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\factory;

use app\modules\api\v1\dto\NotifyDto;

interface AbstractNotifierFactory
{
    public function createNotifier(NotifyDto $dto): NotifierInterface;
}