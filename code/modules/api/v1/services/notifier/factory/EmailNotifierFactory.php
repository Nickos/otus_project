<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\factory;

use app\modules\api\v1\dto\NotifyDto;
use app\modules\api\v1\services\notifier\builder\EmailNotifierBuilder;

class EmailNotifierFactory implements AbstractNotifierFactory
{

    public function createNotifier(NotifyDto $dto): EmailNotifier
    {
        $builder = new EmailNotifierBuilder();
        return $builder
            ->setSubject($dto->getSubject())
            ->setFrom($dto->getFrom())
            ->setTo($dto->getTo())
            ->setText($dto->getText())
            ->build();
    }
}