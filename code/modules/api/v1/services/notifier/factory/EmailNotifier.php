<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\factory;

use Yii;

class EmailNotifier implements NotifierInterface
{
    private string $subject;
    private string $from;
    private array $to;
    private string $text;

    public function __construct(string $subject, string $from, array $to, string $text)
    {
        $this->subject = $subject;
        $this->from = $from;
        $this->to = $to;
        $this->text = $text;
    }

    public function send(): bool
    {
        return Yii::$app->mailer->compose()
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($this->subject)
            ->setHtmlBody($this->text)
            ->send();
    }
}