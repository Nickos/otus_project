<?php
declare(strict_types=1);

namespace app\modules\api\v1\services\notifier\factory;

interface NotifierInterface
{
    public function send();
}