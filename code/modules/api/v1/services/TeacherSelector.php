<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use app\modules\api\v1\models\UserGroup;
use app\modules\api\v1\repositories\GroupApiRepository;
use app\modules\api\v1\repositories\UserApiRepository;
use app\modules\api\v1\services\notifier\Notifier;
use yii\base\Exception;

class TeacherSelector
{
    /**
     * @var \app\modules\api\v1\repositories\UserApiRepository
     */
    private UserApiRepository $users;
    /**
     * @var \app\modules\api\v1\services\TeacherCompatibilityChecker
     */
    private TeacherCompatibilityChecker $teacherChecker;
    /**
     * @var \app\modules\api\v1\repositories\GroupApiRepository
     */
    private GroupApiRepository $groups;
    /**
     * @var \app\modules\api\v1\services\notifier\Notifier
     */
    private Notifier $notifier;

    public function __construct(
        UserApiRepository $users,
        TeacherCompatibilityChecker $teacherChecker,
        GroupApiRepository $groups,
        Notifier $notifier
    )
    {
        $this->users = $users;
        $this->teacherChecker = $teacherChecker;
        $this->groups = $groups;
        $this->notifier = $notifier;
    }

    /**
     * @throws \yii\base\Exception
     */
    public function execute(Group $group): bool
    {
        $teachers = $this->users->getTeacherBySkill($group->main_skill_id);
        if (empty($teachers)) {
            throw new Exception('There are no free teachers');
        }
        foreach ($teachers as $teacher) {
            $groupsWithThisTeacher = $this->groups->getByTeacher($teacher->id);
            if ($this->teacherChecker->execute($group, $teacher) && $groupsWithThisTeacher < User::TEACHER_MAX_GROUPS) {
                $this->groups->addTeacher($group->id, $teacher->id);
                return $this->notifier->notifyTeacherFound($group->users);
            }
        }
        throw new Exception('No teacher found for group');
    }
}