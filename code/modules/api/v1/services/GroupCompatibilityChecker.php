<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use yii\helpers\ArrayHelper;

class GroupCompatibilityChecker
{
    public function execute(array $userSkills, array $groupSkills): bool
    {
        $userSkillsCount = count($userSkills);
        $groupSkillsCount = count($groupSkills);
        if ($userSkillsCount < $groupSkillsCount) {
            $diff = count(array_diff($groupSkills, $userSkills));
            $percent = $diff * 100 / $groupSkillsCount;
        } elseif ($userSkillsCount > $groupSkillsCount) {
            $diff = count(array_diff($userSkills, $groupSkills));
            $percent = $diff * 100 / $userSkillsCount;
        } else {
            $diff = count(array_diff($groupSkills, $userSkills));
            $percent = ($diff == 0) ? 0 : $diff * 100 / $userSkillsCount;
        }
        $percent = 100 - round(floor($percent / 10) * 10);
        return $percent >= User::STUDENT_COMPATIBILITY_PERCENT;
    }
}