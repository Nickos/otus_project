<?php
declare(strict_types=1);

namespace app\modules\api\v1\services;

use app\modules\api\v1\dto\NotifySuccessDto;
use app\modules\api\v1\models\GroupSkill;
use app\modules\api\v1\models\UserSkill;
use app\modules\api\v1\repositories\GroupApiRepository;
use app\modules\api\v1\repositories\UserApiRepository;
use app\modules\api\v1\services\notifier\Notifier;

class GroupSelector
{
    /**
     * @var \app\modules\api\v1\repositories\UserApiRepository
     */
    private UserApiRepository $users;
    /**
     * @var \app\modules\api\v1\repositories\GroupApiRepository
     */
    private GroupApiRepository $groups;
    /**
     * @var \app\modules\api\v1\services\GroupCompatibilityChecker
     */
    private GroupCompatibilityChecker $checker;
    /**
     * @var \app\modules\api\v1\services\notifier\Notifier
     */
    private Notifier $notifier;

    public function __construct(
        UserApiRepository $users,
        GroupApiRepository $groups,
        Notifier $notifier,
        GroupCompatibilityChecker $checker
    )
    {
        $this->users = $users;
        $this->groups = $groups;
        $this->checker = $checker;
        $this->notifier = $notifier;
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function execute(int $userId): bool
    {
        $user = $this->users->get($userId);
        $suitableGroup = $this->groups->selectForUser($user);
        if (!$suitableGroup) {
            return $this->notifier->notifyGroupSelectedFail($user);
        }
        $userSkills = array_map(fn(UserSkill $userSkill) => $userSkill->skill_id, $user->skills);
        $groupSkills = array_map(fn(GroupSkill $groupSkill) => $groupSkill->skill_id, $suitableGroup->skills);
        $areCompatible = $this->checker->execute($userSkills, $groupSkills);
        if ($areCompatible) {
            return $this->notifier->notifyGroupSelectedSuccess($user, $suitableGroup);
        } else {
            return $this->notifier->notifyGroupSelectedFail($user);
        }
    }
}