<?php
declare(strict_types=1);

namespace app\modules\api\v1\helpers\cache;

interface CacheHelperInterface
{
    public function get(string $key);

    public function set(string $key, $value, ?int $duration = null): bool;

    public function getOrSet(string $key, callable $callback, ?int $duration = null);

    public function add(string $key, $value, ?int $duration = null): bool;

    public function delete(string $key): bool;

    public function exist(string $key): bool;
}