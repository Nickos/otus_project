<?php
declare(strict_types=1);

namespace app\modules\api\v1\helpers\cache;

use Yii;

class MemcacheHelper implements CacheHelperInterface
{

    public function get(string $key)
    {
        return Yii::$app->cache->get($key);
    }

    public function set(string $key, $value, ?int $duration = null): bool
    {
        return Yii::$app->cache->set($key, $value, $duration);
    }

    public function getOrSet(string $key, callable $callback, ?int $duration = null)
    {
        return Yii::$app->cache->getOrSet($key, $callback, $duration);
    }

    public function add(string $key, $value, ?int $duration = null): bool
    {
        return Yii::$app->cache->add($key, $value, $duration);
    }

    public function delete(string $key): bool
    {
        return Yii::$app->cache->delete($key);
    }

    public function exist(string $key): bool
    {
        return Yii::$app->cache->exists($key);
    }
}
