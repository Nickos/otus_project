<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use yii\db\ActiveRecord;

class Skill extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'skills';
    }
}