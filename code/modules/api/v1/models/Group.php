<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Group
 * @package app\modules\api\v1\models
 * @property string $name
 * @property int $id
 * @property int $user_count
 * @property int $teacher_id
 * @property int $main_skill_id;
 */
class Group extends ActiveRecord
{
    public const MAX_USER_COUNT = 10;

    public static function tableName(): string
    {
        return 'groups';
    }

    public function getSkills(): ActiveQuery
    {
        return $this->hasMany(GroupSkill::class, ['group_id' => 'id']);
    }

    public function getTeacher(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'teacher_id']);
    }

    public function getUsers(): ActiveQuery
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('users_groups', ['group_id' => 'id']);
    }
}