<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use yii\db\ActiveRecord;

/**
 * Class UserGroup
 * @package app\modules\api\v1\models
 * @property int $user_id
 * @property int $group_id
 */
class UserGroup extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'users_groups';
    }
}