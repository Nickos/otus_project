<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use yii\db\ActiveRecord;

/**
 * Class UserSkill
 * @package app\modules\api\v1\models
 * @property int $user_id
 * @property int $skill_id
 */
class UserSkill extends ActiveRecord
{

    public static function tableName(): string
    {
        return 'users_skills';
    }
}