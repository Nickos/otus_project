<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class User
 * @package app\modules\api\v1\models
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $authKey
 * @property string $accessToken
 * @property int $role_id
 * @property int $main_skill_id
 * @property int $notify_type
 */
class User extends ActiveRecord
{
    public const STUDENT_COMPATIBILITY_PERCENT = 70;

    public const TEACHER_COMPATIBILITY_PERCENT = 90;

    public const TEACHER_MAX_GROUPS = 3;

    public const EMAIL_NOTIFICATION = 1;

    public const STUDENT = 1;

    public static function tableName(): string
    {
        return 'users';
    }

    public function rules(): array
    {
        return [
            [['username', 'email', 'password', 'role_id', 'main_skill_id'], 'required'],
            ['email', 'email'],
        ];
    }

    public function getSkills(): ActiveQuery
    {
        return $this->hasMany(UserSkill::class, ['user_id' => 'id']);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert): bool
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authKey = Yii::$app->security->generateRandomString();
                $this->accessToken = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }
}