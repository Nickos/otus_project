<?php
declare(strict_types=1);

namespace app\modules\api\v1\models;

use yii\db\ActiveRecord;

class Role extends ActiveRecord
{
    public const ROLE_STUDENT = 1;
    public const ROLE_TEACHER = 2;

    public static function tableName(): string
    {
        return 'roles';
    }
}