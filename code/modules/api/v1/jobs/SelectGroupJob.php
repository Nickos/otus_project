<?php
declare(strict_types=1);

namespace app\modules\api\v1\jobs;

use app\modules\api\v1\services\GroupSelector;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class SelectGroupJob extends BaseObject implements JobInterface
{
    public int $userId;
    /**
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function execute($queue)
    {
        $groupSelector = Yii::$container->get(GroupSelector::class);
        $groupSelector->execute($this->userId);
    }
}