<?php
declare(strict_types=1);

namespace app\modules\api\v1\jobs;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\services\TeacherSelector;
use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\queue\JobInterface;

class SelectTeacherJob extends BaseObject implements JobInterface
{
    public Group $group;

    public function execute($queue)
    {
        try {
            $teacherSelector = Yii::$container->get(TeacherSelector::class);
            $teacherSelector->execute($this->group);
        } catch (Exception $exception) {
            Yii::warning($exception->getMessage());
        }
    }
}