<?php
declare(strict_types=1);

namespace app\modules\api\v1\jobs;

use app\modules\api\v1\services\UserToGroupAdder;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class AddUserToGroupJob extends BaseObject implements JobInterface
{
    public int $userId;
    public int $groupId;

    /**
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\Exception
     */
    public function execute($queue)
    {
        $groupAdder = Yii::$container->get(UserToGroupAdder::class);
        $groupAdder->execute($this->userId, $this->groupId);
    }
}