<?php
declare(strict_types=1);

namespace app\modules\api\v1\dto;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use Yii;

class TeacherFoundDto implements NotifyDto
{
    private User $user;

    private string $email;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->email = $user->email;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSubject(): string
    {
        return 'Подбор группы обучения';
    }

    public function getFrom(): string
    {
        return Yii::$app->params['smtp_username'];
    }

    public function getTo(): array
    {
        return [$this->email];
    }

    public function getText(): string
    {
        return 'Для вашей группы был подобран учитель';
    }
}