<?php
declare(strict_types=1);

namespace app\modules\api\v1\dto;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use Yii;
use yii\helpers\Url;

class GroupSelectedSuccessDto implements NotifyDto
{
    private User $user;
    private string $username;
    private int $userId;
    private string $email;
    private string $groupName;
    private int $groupId;

    public function __construct(User $user, Group $group)
    {
        $this->user = $user;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->groupName = $group->name;
        $this->groupId = $group->id;
        $this->userId = $user->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getGroupName(): string
    {
        return $this->groupName;
    }
    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSubject(): string
    {
        return 'Подбор группы обучения';
    }

    public function getFrom(): string
    {
        return Yii::$app->params['smtp_username'];
    }

    public function getTo(): array
    {
        return [$this->email];
    }

    public function getText(): string
    {
        $link = Url::to(['/user/add-to-group',
            'user_id' => $this->getUserId(), 'group_id' => $this->getGroupId()]);
        return "<b>{$this->getUsername()}, для вас подобрана группа \"{$this->getGroupName()}\". 
                Для подтверждения перейдите <a href='{$link}'>по ссылке</a></b>";
    }
}