<?php
declare(strict_types=1);

namespace app\modules\api\v1\dto;

use app\modules\api\v1\models\User;

interface NotifyDto
{
    public function getUser(): User;

    public function getSubject(): string;

    public function getFrom(): string;

    public function getTo(): array;

    public function getText(): string;
}