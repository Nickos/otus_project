<?php
declare(strict_types=1);

namespace app\modules\api\v1\dto;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use Yii;

class GroupSelectedFailDto implements NotifyDto
{
    private User $user;
    private string $username;
    private int $userId;
    private string $email;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->userId = $user->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSubject(): string
    {
        return 'Подбор группы обучения';
    }

    public function getFrom(): string
    {
        return Yii::$app->params['smtp_username'];
    }

    public function getTo(): array
    {
        return [$this->email];
    }

    public function getText(): string
    {
        return "<b>К сожалению, не удалось подобрать подходящую группу.</b>";
    }
}