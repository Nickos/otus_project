<?php
declare(strict_types=1);

namespace app\modules\api\v1\dto;

use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use Yii;

class UserAddedToGroupDto implements NotifyDto
{
    private User $user;
    private string $email;
    private string $groupName;

    public function __construct(User $user, Group $group)
    {
        $this->user = $user;
        $this->email = $user->email;
        $this->groupName = $group->name;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSubject(): string
    {
        return 'Подбор группы обучения';
    }

    public function getFrom(): string
    {
        return Yii::$app->params['smtp_username'];
    }

    public function getTo(): array
    {
        return [$this->email];
    }

    public function getText(): string
    {
        return 'Вы были добавлены в группу ' . '<b>\'' . $this->groupName . '\'</b>';
    }
}