<?php
declare(strict_types=1);

namespace app\modules\api\v1\controllers;

use app\modules\api\v1\actions\user\CreateAction;
use app\modules\api\v1\jobs\AddUserToGroupJob;
use app\modules\api\v1\services\UserToGroupAdder;
use Yii;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    /**
     * @OA\Info(title="My First API", version="0.1")
     */
    public $modelClass = 'app\modules\api\v1\models\User';

    public function actions(): array
    {
        $actions = [
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];

        return array_merge(parent::actions(), $actions);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/users",
     *     summary="Adds a new user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="username",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="role_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="main_skill_id",
     *                     type="integer"
     *                 ),
     *                 example={"username": "Николай", "email": "kleymenov68@yandex.ru", "role_id": 1, "main_skill_id": 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             oneOf={
     *                 @OA\Schema(ref="#/components/schemas/Result"),
     *                 @OA\Schema(type="boolean")
     *             }
     *         )
     *     )
     * )
     */

    /**
     * @OA\Schema(
     *  schema="Result",
     *  title="Sample schema for using references",
     * 	@OA\Property(
     *        property="status",
     *        type="string"
     *    ),
     * 	@OA\Property(
     *        property="error",
     *        type="string"
     *    )
     * )
     */
    public function actionAddToGroup(): array
    {
        $params = Yii::$app->request->post();
        $userId = (int)$params['userId'];
        $groupId = (int)$params['groupId'];
        Yii::$app->selectGroupQueue->push(new AddUserToGroupJob([
            'userId' => $userId,
            'groupId' => $groupId
        ]));
        $response = Yii::$app->getResponse();
        $response->setStatusCode(201);
        return ['success' => true];
    }
}