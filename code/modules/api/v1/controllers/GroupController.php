<?php
declare(strict_types=1);

namespace app\modules\api\v1\controllers;

use app\modules\api\v1\jobs\SelectGroupJob;
use Yii;
use yii\rest\ActiveController;

class GroupController extends ActiveController
{
    public $modelClass = 'app\modules\api\v1\models\Group';

    public function actionSelect(): array
    {
        $userId = (int)Yii::$app->request->post('userId');
        Yii::$app->selectGroupQueue->push(new SelectGroupJob([
            'userId' => $userId,
        ]));
        $response = Yii::$app->getResponse();
        $response->setStatusCode(201);
        return ['success' => true];
    }
}