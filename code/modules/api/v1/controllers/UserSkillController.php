<?php
declare(strict_types=1);

namespace app\modules\api\v1\controllers;

use app\modules\api\v1\actions\userSkill\CreateAction;
use yii\rest\ActiveController;

class UserSkillController extends ActiveController
{
    public $modelClass = 'app\modules\api\v1\models\UserSkill';

    public function actions(): array
    {
        $actions = [
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];

        return array_merge(parent::actions(), $actions);
    }
}