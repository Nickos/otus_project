<?php
declare(strict_types=1);

namespace app\modules\api\v1\controllers;

use yii\rest\ActiveController;

class RoleController extends ActiveController
{
    public $modelClass = 'app\modules\api\v1\models\Role';
}