<?php
declare(strict_types=1);

namespace app\modules\api\v1\controllers;

use yii\rest\ActiveController;

class SkillController extends ActiveController
{
    public $modelClass = 'app\modules\api\v1\models\Skill';

    public function actionMain()
    {
        $model = new $this->modelClass;
        return $model::find()->where(['is_main' => 1])->all();
    }
}