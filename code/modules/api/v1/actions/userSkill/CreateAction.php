<?php
declare(strict_types=1);

namespace app\modules\api\v1\actions\userSkill;

use app\modules\api\v1\models\UserSkill;
use Yii;
use yii\rest\Action;

class CreateAction extends Action
{
    /**
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function run(): array
    {
            if ($this->checkAccess) {
                call_user_func($this->checkAccess, $this->id);
            }

            $userId = Yii::$app->getRequest()->getBodyParams()['userId'];
            $skills = Yii::$app->getRequest()->getBodyParams()['skills'];
            Yii::$app->db->createCommand()->batchInsert(UserSkill::tableName(),
                ['user_id', 'skill_id'], array_map(function ($skill) use ($userId) {
                    return [
                        $userId,
                        $skill
                    ];
                }, $skills))->execute();
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            return ['success' => true];
    }
}