<?php
declare(strict_types=1);

namespace app\modules\api\v1\actions\user;

use app\modules\api\v1\models\User;
use Yii;
use yii\helpers\Url;
use yii\rest\Action;
use yii\web\ServerErrorHttpException;

class CreateAction extends Action
{
    public string $viewAction = 'view';

    public string $scenario = 'create';

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\ServerErrorHttpException
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        $model = new User();
        $model->setAttributes($requestParams);
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            return $model->getPrimaryKey();
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }
    }
}