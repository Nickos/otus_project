<?php
declare(strict_types=1);

namespace app\modules\api\v1\repositories;

use app\modules\api\v1\helpers\cache\CacheHelperInterface;
use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use Yii;
use yii\web\NotFoundHttpException;

class GroupApiRepository
{
    /**
     * @var \app\modules\api\v1\helpers\cache\CacheHelperInterface
     */
    private CacheHelperInterface $cache;

    public function __construct(CacheHelperInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function get(int $id): ?Group
    {
        $group = $this->cache->getOrSet('group_' . $id, function () use ($id) {
            return Group::findOne($id);
        });
        if (!$group) {
            throw new NotFoundHttpException('Group not found.');
        }
        return $group;
    }

    /**
     * @throws \yii\db\Exception
     */
    public function updateCount(int $id): int
    {
        $count = Yii::$app->db
            ->createCommand("UPDATE `groups` SET `user_count` = `user_count` + 1 WHERE `id` = '$id' ")
            ->execute();
        $this->cache->delete('group_' . $id);
        return $count;
    }


    public function selectForUser(User $user): ?Group
    {
        return $this->cache->getOrSet('group_with_main_skill_' . $user->main_skill_id, function () use ($user) {
           return Group::find()
               ->where(['main_skill_id' => $user->main_skill_id])
               ->one();
        });
    }

    public function getByTeacher(int $teacherId): int
    {
        return (int)Group::find()->where(['teacher_id' => $teacherId])->count();
    }

    public function addTeacher(int $groupId, int $teacherId): int
    {
        return Group::updateAll(['teacher_id' => $teacherId], "id = $groupId");
    }
}