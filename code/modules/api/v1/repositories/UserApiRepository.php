<?php
declare(strict_types=1);

namespace app\modules\api\v1\repositories;

use app\modules\api\v1\models\Role;
use app\modules\api\v1\models\User;
use yii\web\NotFoundHttpException;

class UserApiRepository
{
    /**
     * @throws \yii\web\NotFoundHttpException
     */
    public function get(int $userId): User
    {
        $user = User::find()
            ->where(['id' => $userId])
            ->with(['skills'])
            ->one();
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }
        return $user;
    }

    public function getTeacherBySkill(int $skillId): array
    {
        return User::find()
            ->where(['main_skill_id' => $skillId])
            ->andWhere(['role_id' => Role::ROLE_TEACHER])
            ->with('skills')
            ->all();
    }
}