<?php
declare(strict_types=1);

namespace app\modules\api\v1\repositories;

use app\modules\api\v1\models\UserGroup;

class UserGroupApiRepository
{
    public function create(int $userId, int $groupId): bool
    {
        $model = new UserGroup();
        $model->user_id = $userId;
        $model->group_id = $groupId;
        return $model->save();
    }
}