<?php
declare(strict_types=1);

use yii\rest\UrlRule;

return  [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'baseUrl' => 'https://dikiykach.ru',
    'rules' => [
        '<controller:\w+>/<action\w+>' => '<controller>/<action>',
        '/' => 'user/register-form',
        'api/v1/skills/main' => 'v1/skill/main',
        [
            'class' => UrlRule::class,
            'controller' => ['v1/role', 'v1/user', 'v1/user-skill', 'v1/user-group', 'v1/skill'],
            'prefix' => 'api',
        ],
        [
            'class' => UrlRule::class,
            'controller' => ['v1/group'],
            'prefix' => 'api',
            'extraPatterns' => [
                'POST select' => 'select'
            ]
        ],
        [
            'class' => UrlRule::class,
            'controller' => ['v1/user'],
            'prefix' => 'api',
            'extraPatterns' => [
                'POST add-to-group' => 'add-to-group'
            ]
        ]
    ],
];