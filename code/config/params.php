<?php

use yii\helpers\ArrayHelper;

$params = [

];

return ArrayHelper::merge(
    $params,
    file_exists(__DIR__ . '/params-local.php') ? require(__DIR__ . '/params-local.php') : []
);