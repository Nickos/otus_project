<?php

use yii\helpers\ArrayHelper;

$db = [
    'class' => 'yii\db\Connection',
];

return ArrayHelper::merge(
    $db,
    file_exists(__DIR__ . '/db-local.php') ? require(__DIR__ . '/db-local.php') : []
);