<?php
declare(strict_types=1);

return [
    'class' => 'yii\swiftmailer\Mailer',
    'useFileTransport' => true,
];
