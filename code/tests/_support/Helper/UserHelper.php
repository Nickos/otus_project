<?php
declare(strict_types=1);

namespace Helper;

use ApiTester;
use Yii;

class UserHelper
{
    public static function createUser(ApiTester $I)
    {
        $email = 'kleymenov68@yandex.ru';
        $I->haveInDatabase('users', [
            'username' => 'Nick',
            'email' => $email,
            'password' => Yii::$app->security->generatePasswordHash('12345'),
            'role_id' => 1,
            'main_skill_id' => 1,
        ]);
        return $I->grabFromDatabase('users', 'id', ['email' => $email]);
    }
}