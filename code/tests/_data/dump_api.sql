-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июл 10 2021 г., 22:23
-- Версия сервера: 8.0.25-0ubuntu0.20.04.1
-- Версия PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+03:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `otus`
--

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups`
(
    `id`            int NOT NULL,
    `name`          varchar(255) DEFAULT NULL,
    `main_skill_id` int          DEFAULT NULL,
    `user_count`    tinyint      DEFAULT 0,
    `teacher_id`    int          DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `main_skill_id`, `user_count`, `teacher_id`)
VALUES (1, 'PHP разработка', 1, 10, 33),
       (2, 'Fronted разработка', 2, 0, NULL),
       (3, 'Мобильная разработка на flutter', 3, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `groups_skills`
--

CREATE TABLE `groups_skills`
(
    `group_id` int NOT NULL,
    `skill_id` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

--
-- Дамп данных таблицы `groups_skills`
--

INSERT INTO `groups_skills` (`group_id`, `skill_id`)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (2, 10),
       (1, 13),
       (2, 15),
       (2, 16),
       (3, 17),
       (3, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration`
(
    `version`    varchar(180) NOT NULL,
    `apply_time` int DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;


-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles`
(
    `id`   int NOT NULL,
    `name` varchar(30) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`)
VALUES (1, 'Ученик'),
       (2, 'Преподаватель');

-- --------------------------------------------------------

--
-- Структура таблицы `skills`
--

CREATE TABLE `skills`
(
    `id`      int NOT NULL,
    `name`    varchar(50) DEFAULT NULL,
    `is_main` tinyint     DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

--
-- Дамп данных таблицы `skills`
--

CREATE TABLE `users`
(
    `id`            int NOT NULL,
    `username`      varchar(50)  DEFAULT NULL,
    `password`      varchar(255) DEFAULT NULL,
    `authKey`       varchar(255) DEFAULT NULL,
    `accessToken`   varchar(255) DEFAULT NULL,
    `email`         varchar(255) DEFAULT NULL,
    `role_id`       int          DEFAULT NULL,
    `busy`          tinyint(1)   DEFAULT 0 ,
    `main_skill_id` int NOT NULL,
    `notify_type`   tinyint(1)   DEFAULT 1
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

INSERT INTO `skills` (`id`, `name`, `is_main`)
VALUES (1, 'PHP', 1),
       (2, 'JavaScript', 1),
       (3, 'Flutter', 1),
       (4, 'Nginx', 0),
       (5, 'MySQL', 0),
       (6, 'Docker', 0),
       (7, 'Yii2', 0),
       (8, 'Laravel', 0),
       (9, 'Symphony', 1),
       (10, 'Vue', 0),
       (11, 'Java', 0),
       (12, 'Go', 1),
       (13, 'Redis', 0),
       (14, 'Python', 0),
       (15, 'ReactJS', 0),
       (16, 'css', 0),
       (17, 'Dart', 0),
       (18, 'React Native', 0);

--
-- Дамп данных таблицы `users`
--
--
-- Структура таблицы `users_groups`
--

CREATE TABLE `users_groups`
(
    `user_id`  int NOT NULL,
    `group_id` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;

-- --------------------------------------------------------

--
-- Структура таблицы `users_skills`
--

CREATE TABLE `users_skills`
(
    `user_id`  int NOT NULL,
    `skill_id` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;


--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
    ADD PRIMARY KEY (`id`),
    ADD KEY `group_teacher_id` (`teacher_id`),
    ADD KEY `group_main_skill_id` (`main_skill_id`);

--
-- Индексы таблицы `groups_skills`
--
ALTER TABLE `groups_skills`
    ADD PRIMARY KEY (`group_id`, `skill_id`),
    ADD KEY `group_skill_skill_id` (`skill_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
    ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
    ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `skills`
--
ALTER TABLE `skills`
    ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `email` (`email`),
    ADD KEY `user_role_id` (`role_id`),
    ADD KEY `user_main_skill_id` (`main_skill_id`);

--
-- Индексы таблицы `users_groups`
--
ALTER TABLE `users_groups`
    ADD PRIMARY KEY (`user_id`, `group_id`),
    ADD KEY `users_groups_group_id` (`group_id`);

--
-- Индексы таблицы `users_skills`
--
ALTER TABLE `users_skills`
    ADD PRIMARY KEY (`user_id`, `skill_id`),
    ADD KEY `user_skill_skill_id` (`skill_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 4;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3;

--
-- AUTO_INCREMENT для таблицы `skills`
--
ALTER TABLE `skills`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 19;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 37;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `groups`
--
ALTER TABLE `groups`
    ADD CONSTRAINT `group_main_skill_id` FOREIGN KEY (`main_skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `group_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `groups_skills`
--
ALTER TABLE `groups_skills`
    ADD CONSTRAINT `group_skill_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
    ADD CONSTRAINT `group_skill_skill_id` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
    ADD CONSTRAINT `user_main_skill_id` FOREIGN KEY (`main_skill_id`) REFERENCES `skills` (`id`),
    ADD CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `users_groups`
--
ALTER TABLE `users_groups`
    ADD CONSTRAINT `users_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `users_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users_skills`
--
ALTER TABLE `users_skills`
    ADD CONSTRAINT `user_skill_skill_id` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_skill_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;