<?php

namespace yii\test\functional;

use ApiTester;
use Helper\UserHelper;

class UserCest
{
    public function createUser(ApiTester $I)
    {
        $I->wantToTest('inserting data to users table');
        $userId = UserHelper::createUser($I);
        $I->seeInDatabase('users', ['id' => $userId]);
    }
}
