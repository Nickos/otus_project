<?php

namespace yii\test\functional;

use ApiTester;
use Helper\UserHelper;

class UserSkillCest
{
    public function addUserSkills(ApiTester $I)
    {
        $I->wantToTest('inserting data to users_skills table');
        $userId = UserHelper::createUser($I);
        $skills = [3, 4];
        foreach ($skills as $skill) {
            $I->haveInDatabase('users_skills', [
                'skill_id' => $skill,
                'user_id' => $userId
            ]);
        }
        $I->seeNumRecords(count($skills), 'users_skills', ['user_id' => $userId]);
    }
}
