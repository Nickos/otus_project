<?php

namespace yii\test\functional;

use ApiTester;
use app\modules\api\v1\models\Group;
use app\modules\api\v1\models\User;
use app\modules\api\v1\models\UserSkill;
use app\modules\api\v1\services\GroupCompatibilityChecker;
use Codeception\Stub;
use Codeception\Test\Unit;
use Helper\UserHelper;

class UserGroupCest
{
    public function addUserToGroup(ApiTester $I)
    {
        $I->wantToTest('add user to group table');
        $userId = UserHelper::createUser($I);
        $groupId = 1;
        $I->haveInDatabase('users_groups', ['user_id' => $userId, 'group_id' => $groupId]);
        $I->seeNumRecords(1, 'users_groups', ['user_id' => $userId]);
    }

    public function selectGroupSuccess(ApiTester $I)
    {
        $I->expect('compatibility will be true');
        $userSkills = [1, 4, 5, 6, 9];
        $groupSkills = [1, 4, 5, 6, 9, 13];
        $compatibilityChecker = Stub::make(GroupCompatibilityChecker::class);
        $compatibilityPercent = $compatibilityChecker->execute($userSkills, $groupSkills);
        Unit::assertTrue($compatibilityPercent >= User::STUDENT_COMPATIBILITY_PERCENT);
    }

    public function selectGroupFailed(ApiTester $I)
    {
        $I->expect('compatibility will be false');
        $userSkills = [1, 4, 5];
        $groupSkills = [1, 4, 5, 6, 9, 13];
        $compatibilityChecker = Stub::make(GroupCompatibilityChecker::class);
        $compatibilityPercent = $compatibilityChecker->execute($userSkills, $groupSkills);
        Unit::assertTrue($compatibilityPercent <= User::STUDENT_COMPATIBILITY_PERCENT);
    }
}
