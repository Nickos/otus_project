<?php
declare(strict_types=1);

namespace app\frontend\models;

use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
    public ?string $username = null;
    public ?string $email = null;
    public ?string $password = null;
    public ?int $role_id = null;
    public ?int $main_skill_id = null;
    public ?int $rememberMe = null;

    public function rules(): array
    {
        return [
            [['username', 'email', 'password', 'role_id', 'main_skill_id'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @throws \yii\base\Exception
     */
    public function prepare()
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
    }
}