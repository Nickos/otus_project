<?php
declare(strict_types=1);

namespace app\frontend\models;

use yii\base\Model;

class SkillForm extends Model
{
    public ?array $skills = [];
    public ?int $userId = null;

    public function rules(): array
    {
        return [
            [['skills'], 'safe']
        ];
    }

    public function getSkills(): array
    {
        return $this->skills;
    }
}