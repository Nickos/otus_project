<?php
declare(strict_types=1);

namespace app\frontend\controllers;

use app\frontend\dto\UserRegisterDto;
use app\frontend\models\RegisterForm;
use app\frontend\models\SkillForm;
use app\frontend\repositories\RoleRepository;
use app\frontend\repositories\SkillRepository;
use app\frontend\services\UserRegister;
use app\frontend\services\UserToGroup;
use GuzzleHttp\Exception\GuzzleException;
use Yii;
use yii\web\Controller;

class UserController extends Controller
{
    private RoleRepository $roles;
    private SkillRepository $skills;
    private UserRegister $registerService;
    public $layout = '@frontend/views/layouts/main.php';
    /**
     * @var \app\frontend\services\UserToGroup
     */
    private UserToGroup $userAdder;

    public function __construct(
        $id,
        $module,
        RoleRepository $roles,
        SkillRepository $skills,
        UserRegister $registerService,
        UserToGroup $userAdder,
        $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->roles = $roles;
        $this->skills = $skills;
        $this->registerService = $registerService;
        $this->userAdder = $userAdder;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function actionRegisterForm(): string
    {
        $userForm = new RegisterForm();
        $skillForm = new SkillForm();
        $roles = $this->roles->get();
        $mainSkills = $this->skills->getMain();
        $skills = $this->skills->get();
        return $this->render('registration-form',
            ['userForm' => $userForm, 'roles' => $roles, 'skills' => $skills,
                'mainSkills' => $mainSkills, 'skillForm' => $skillForm]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException|\yii\base\Exception
     */
    public function actionRegister(): string
    {
        if (Yii::$app->request->isPost) {
            $dto = new UserRegisterDto(Yii::$app->request->post(), new RegisterForm(), new SkillForm());
            $success = $this->registerService->execute($dto);
            if ($success) {
                return $this->render('register-confirm');
            } else {
                return $this->render('register-failed');
            }
        }
    }

    public function actionAddToGroup(): string
    {
        try {
            if (Yii::$app->request->isGet) {
                $userId = (int)Yii::$app->request->get('user_id');
                $groupId = (int)Yii::$app->request->get('group_id');
                $result = $this->userAdder->execute($userId, $groupId);
                if ($result) {
                    return $this->render('enrollment-confirm');
                }
            }
        } catch (GuzzleException $e) {
            return $this->render('enrollment-failed');
        }
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/views/user');
    }
}