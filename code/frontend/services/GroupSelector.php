<?php
declare(strict_types=1);

namespace app\frontend\services;

use app\frontend\repositories\GroupRepository;

class GroupSelector
{
    /**
     * @var \app\frontend\repositories\GroupRepository
     */
    private GroupRepository $groups;

    public function __construct(GroupRepository $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(int $userId)
    {
        $this->groups->selectGroupForUser($userId);
    }
}