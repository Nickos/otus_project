<?php
declare(strict_types=1);

namespace app\frontend\services;

use app\frontend\repositories\UserRepository;

class UserToGroup
{
    /**
     * @var \app\frontend\repositories\UserRepository
     */
    private UserRepository $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute($userId, $groupId)
    {
        return $this->users->addToGroup($userId, $groupId);
    }
}