<?php
declare(strict_types=1);

namespace app\frontend\services;

use app\frontend\dto\UserRegisterDto;
use app\frontend\models\User;
use app\frontend\repositories\UserRepository;

class UserRegister
{
    /**
     * @var UserRepository
     */
    private UserRepository $users;
    /**
     * @var GroupSelector
     */
    private GroupSelector $selector;

    public function __construct(UserRepository $users, GroupSelector $selector)
    {
        $this->users = $users;
        $this->selector = $selector;
    }

    /**
     * @throws \yii\base\Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function execute(UserRegisterDto $dto): bool
    {
            $userData = $dto->getUserForm();
            if (!$userData->validate()) {
                throw new \DomainException('User data does not validate');
            }
            $userData->prepare();
            $userId = $this->users->store($userData);
            $userSkills = $dto->getSkillForm();
            if (!empty($userSkills->getSkills())) {
                $this->users->storeSkills($userSkills, $userId);
            }
            if ((int)$userData->role_id === User::STUDENT) {
                $this->selector->execute($userId);
            }
            return true;
    }
}