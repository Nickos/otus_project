<?php
declare(strict_types=1);
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $userForm app\frontend\models\RegisterForm */
/* @var $roles array */
/* @var $skills array */
/* @var $mainSkills array */
/* @var $skillForm \app\frontend\models\SkillForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
            'action' => 'user/register',
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($userForm, 'username')->textInput(['autofocus' => true])->label('Имя') ?>

    <?= $form->field($userForm, 'email')->textInput() ?>

    <?= $form->field($userForm, 'password')->passwordInput()->label('Пароль') ?>

    <?= $form->field($userForm, 'role_id')->radioList(ArrayHelper::map($roles, 'id', 'name'))->label('Статус') ?>

    <?= $form->field($userForm, 'main_skill_id')->radioList(ArrayHelper::map($mainSkills, 'id', 'name'))->label('Основной навык') ?>

    <?= $form->field($skillForm, 'skills')->checkboxList(ArrayHelper::map($skills, 'id', 'name'))->label('Доп навыки') ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>