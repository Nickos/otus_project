<?php
declare(strict_types=1);

namespace app\frontend\dto;

use app\frontend\models\RegisterForm;
use app\frontend\models\SkillForm;

class UserRegisterDto
{
    public RegisterForm $userForm;
    public SkillForm $skillForm;

    public function __construct(array $post, RegisterForm $userForm, SkillForm $skillForm)
    {
        $this->userForm = $userForm;
        $this->skillForm = $skillForm;
        $this->userForm->load($post);
        $this->skillForm->load(($post['SkillForm']['skills'] != '') ? $post : []);
    }

    public function getUserForm(): RegisterForm
    {
        return $this->userForm;
    }

    public function getSkillForm(): SkillForm
    {
        return $this->skillForm;
    }
}