<?php
declare(strict_types=1);

namespace app\frontend\repositories;

use GuzzleHttp\Client;
use Yii;

class BaseRemoteRepository
{
    protected Client $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => Yii::$app->params['apiUri'] . Yii::$app->params['apiVersion']]);
    }

    protected function getResponse($request)
    {
        return json_decode($request->getBody()->getContents(), true);
    }
}