<?php
declare(strict_types=1);

namespace app\frontend\repositories;

use app\frontend\models\RegisterForm;
use app\frontend\models\SkillForm;

class UserRepository extends BaseRemoteRepository
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(RegisterForm $data)
    {
        $request = $this->client->request('POST', 'users', ['form_params' => $data]);
        return $this->getResponse($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function storeSkills(SkillForm $skills, int $userId)
    {
        $request = $this->client->request('POST', 'user-skills',
            ['form_params' => ['skills' => $skills->getSkills(), 'userId' => $userId]]);
        return $this->getResponse($request);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addToGroup(int $userId, int $groupId)
    {
        $request = $this->client->request('POST', 'users/add-to-group',
            ['form_params' => ['groupId' => $groupId, 'userId' => $userId]]);
        return $this->getResponse($request);
    }
}