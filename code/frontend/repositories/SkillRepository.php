<?php
declare(strict_types=1);

namespace app\frontend\repositories;

use app\modules\api\v1\helpers\cache\CacheHelperInterface;
use Yii;

class SkillRepository extends BaseRemoteRepository
{
    /**
     * @var \app\modules\api\v1\helpers\cache\CacheHelperInterface
     */
    private CacheHelperInterface $cache;

    public function __construct(CacheHelperInterface $cache)
    {
        parent::__construct();
        $this->cache = $cache;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->cache->getOrSet('all_skills', function () {
            $request = $this->client->request('GET', 'skills');
            return $this->getResponse($request);
        }, Yii::$app->params['skill_cache_duration']);
    }

    public function getMain()
    {
        return $this->cache->getOrSet('main_skills', function () {
            $request = $this->client->request('GET', 'skills/main');
            return $this->getResponse($request);
        }, Yii::$app->params['skill_cache_duration']);
    }
}