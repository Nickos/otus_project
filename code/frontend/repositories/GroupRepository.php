<?php
declare(strict_types=1);

namespace app\frontend\repositories;

class GroupRepository extends BaseRemoteRepository
{
    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function selectGroupForUser(int $userId)
    {
        $request = $this->client->request('POST', "groups/select", ['form_params' => ['userId' => $userId]]);
        return $this->getResponse($request);
    }
}