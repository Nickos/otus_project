<?php
declare(strict_types=1);

namespace app\frontend\repositories;

use app\modules\api\v1\helpers\cache\CacheHelperInterface;
use Yii;

class RoleRepository extends BaseRemoteRepository
{
    /**
     * @var \app\modules\api\v1\helpers\cache\CacheHelperInterface
     */
    private CacheHelperInterface $cache;

    public function __construct(CacheHelperInterface $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    public function get()
    {
        return $this->cache->getOrSet('all_roles', function () {
            $request = $this->client->request('GET', 'roles');
            return $this->getResponse($request);
        }, Yii::$app->params['roles_cache_duration']);
    }
}