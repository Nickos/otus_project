<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m210629_071223_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50),
            'password' => $this->string(),
            'authKey' => $this->string(),
            'accessToken' => $this->string(),
            'email' => $this->string()->unique(),
            'role_id' => $this->integer(),
            'main_skill_id' => $this->integer()->notNull(),
            'notify_type' => $this->tinyInteger(1)->defaultValue(1)
        ]);

        $this->addForeignKey('user_role_id', 'users', 'role_id', 'roles', 'id');
        $this->addForeignKey('user_main_skill_id', 'users', 'main_skill_id', 'skills', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
