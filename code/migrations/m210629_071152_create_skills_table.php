<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%skills}}`.
 */
class m210629_071152_create_skills_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%skills}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'is_main' => $this->tinyInteger()->defaultValue(0)
        ]);
        $skills = ['PHP', 'JavaScript', 'Flutter', 'Nginx', 'MySQL', 'Docker', 'Yii2', 'Laravel', 'Symphony', 'Vue', 'Java', 'Go', 'Redis',
            'Python', 'ReactJS', 'css', 'Dart', 'React Native'];
        $mainSkills = ['PHP', 'Flutter', 'JavaScript', 'Go', 'Symphony'];
        foreach ($skills as $skill) {
            $isMain = in_array($skill, $mainSkills);
            $this->insert('skills', ['name' => $skill, 'is_main' => $isMain]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%skills}}');
    }
}
