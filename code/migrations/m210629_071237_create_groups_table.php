<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%groups}}`.
 */
class m210629_071237_create_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'main_skill_id' => $this->integer(),
            'user_count' => $this->tinyInteger()->defaultValue(0),
            'teacher_id' => $this->integer()
        ]);

        $this->addForeignKey('group_teacher_id', 'groups', 'teacher_id', 'users', 'id');
        $this->addForeignKey('group_main_skill_id', 'groups', 'main_skill_id', 'skills', 'id', 'CASCADE');
        $this->insert('groups', ['name' => 'PHP разработка', 'main_skill_id' => 1]);
        $this->insert('groups', ['name' => 'Fronted разработка', 'main_skill_id' => 2]);
        $this->insert('groups', ['name' => 'Мобильная разработка на flutter', 'main_skill_id' => 3]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%groups}}');
    }
}
