<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_skills}}`.
 */
class m210629_071255_create_users_skills_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users_skills}}', [
            'user_id' => $this->integer(),
            'skill_id' => $this->integer()
        ]);
        $this->addForeignKey('user_skill_user_id', 'users_skills', 'user_id', 'users', 'id', 'CASCADE');
        $this->addForeignKey('user_skill_skill_id', 'users_skills', 'skill_id', 'skills', 'id', 'CASCADE');
        $this->addPrimaryKey('user_skill_pk', 'users_skills', ['user_id', 'skill_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_skills}}');
    }
}
