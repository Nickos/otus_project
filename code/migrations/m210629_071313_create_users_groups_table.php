<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_groups}}`.
 */
class m210629_071313_create_users_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users_groups}}', [
            'user_id' => $this->integer(),
            'group_id' => $this->integer()
        ]);
        $this->addForeignKey('users_groups_user_id', 'users_groups', 'user_id', 'users', 'id', 'CASCADE');
        $this->addForeignKey('users_groups_group_id', 'users_groups', 'group_id', 'groups', 'id', 'CASCADE');
        $this->addPrimaryKey('users_groups_pk', 'users_groups', ['user_id', 'group_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users_groups}}');
    }
}
