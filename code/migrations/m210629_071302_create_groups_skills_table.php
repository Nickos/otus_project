<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%groups_skills}}`.
 */
class m210629_071302_create_groups_skills_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%groups_skills}}', [
            'group_id' => $this->integer(),
            'skill_id' => $this->integer()
        ]);
        $this->addForeignKey('group_skill_group_id', 'groups_skills', 'group_id', 'groups', 'id');
        $this->addForeignKey('group_skill_skill_id', 'groups_skills', 'skill_id', 'skills', 'id');
        $this->addPrimaryKey('group_skill_pk', 'groups_skills', ['group_id', 'skill_id']);
        $phpGroupsSkills = [1, 4, 5, 6, 13];
        $frontendGroupSkills = [2, 10, 15, 16];
        $flutterGroupSkills = [3, 17, 18];
        foreach ($phpGroupsSkills as $skill) {
            $this->insert('groups_skills', [
                'group_id' => 1,
                'skill_id' => $skill
            ]);
        }
        foreach ($frontendGroupSkills as $skill) {
            $this->insert('groups_skills', [
                'group_id' => 2,
                'skill_id' => $skill
            ]);
        }
        foreach ($flutterGroupSkills as $skill) {
            $this->insert('groups_skills', [
                'group_id' => 3,
                'skill_id' => $skill
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%groups_skills}}');
    }
}
