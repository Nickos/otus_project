<?php
declare(strict_types=1);

namespace app\bootstrap;

use app\modules\api\v1\helpers\cache\CacheHelperInterface;
use app\modules\api\v1\helpers\cache\MemcacheHelper;
use app\modules\api\v1\repositories\GroupApiRepository;
use app\modules\api\v1\repositories\UserApiRepository;
use app\modules\api\v1\repositories\UserGroupApiRepository;
use app\modules\api\v1\services\GroupCompatibilityChecker;
use app\modules\api\v1\services\notifier\Notifier;
use app\modules\api\v1\services\TeacherCompatibilityChecker;
use app\modules\api\v1\services\UserToGroupAdder;
use app\repositories\GroupRepository;
use app\repositories\RoleRepository;
use app\repositories\SkillRepository;
use app\repositories\UserRepository;
use app\services\GroupSelector;
use app\services\UserRegister;
use app\services\UserToGroup;
use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $container = Yii::$container;

        $container->setSingletons([
            RoleRepository::class => RoleRepository::class,
            SkillRepository::class => SkillRepository::class,
            UserRepository::class => UserRepository::class,
            UserRegister::class => UserRegister::class,
            GroupSelector::class => GroupSelector::class,
            GroupRepository::class => GroupRepository::class,
            UserApiRepository::class => UserApiRepository::class,
            GroupApiRepository::class => GroupApiRepository::class,
            GroupCompatibilityChecker::class => GroupCompatibilityChecker::class,
            UserToGroupAdder::class => UserToGroupAdder::class,
            UserGroupApiRepository::class => UserGroupApiRepository::class,
            UserToGroup::class => UserToGroup::class,
            CacheHelperInterface::class => MemcacheHelper::class,
            TeacherCompatibilityChecker::class => TeacherCompatibilityChecker::class,
            Notifier::class => Notifier::class
        ]);
    }
}